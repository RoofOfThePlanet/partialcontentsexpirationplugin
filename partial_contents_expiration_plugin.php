<?php

/*
Plugin Name: Partial Contents Expiration Plugin
Plugin URI: https://bitbucket.org/RoofOfThePlanet/partialcontentsexpirationplugin
Description: Offers simple protection for expired contents.
Author: RoofOfThePlanet
Author URI: https://bitbucket.org/RoofOfThePlanet/
Version: 0.1
*/

/*

Copyright 2019-2020 RoofOfThePlanet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    [http://www.apache.org/licenses/LICENSE-2.0]

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

class Partial_Contents_Expiration {
	public function __construct() {
		add_shortcode( 'pcep' , array( &$this , 'protect_expired_contents' ) );
	}

	private function is_valid_date_time_format( $date , $format = 'Y-m-d\TH:i:sP' ) {
		$d = DateTime::createFromFormat( $format, $date );
		return $d && $d->format($format) == $date;
	}
	private function parse_date_time( $time , $datefmt , $timefmt , $adj ) {
		$datetimefmt = $datefmt . ' ' . $timefmt;
		if ($this->is_valid_date_time_format( $time , $datetimefmt )) {
			return DateTime::createFromFormat( $datetimefmt . '|' , $time , $adj->getTimezone() );
		} elseif ($this->is_valid_date_time_format( $time , $datefmt )) {
			return DateTime::createFromFormat( $datefmt . '|' , $time , $adj->getTimezone() );
		} elseif ($this->is_valid_date_time_format( $time , $timefmt )) {
			$temp = DateTime::createFromFormat( $timefmt , $time , $adj->getTimezone() );
			$temp = $adj->setTime(
				intval($temp->format('H')),
				intval($temp->format('i')),
				intval($temp->format('s'))
			);
			return $temp->setTimezone( $adj->getTimezone() );
		} else {
			return null;
		}
	}

	public function protect_expired_contents( $attrs , $contents , $codes ) {
		$default_date_format = get_option('date_format');
		$default_time_format = get_option('time_format');
		$default_timezone = wp_timezone();
		$contents = do_shortcode( $contents );
		$attrs = shortcode_atts( array(
			'start' => '',
			'end' => '',
			'format' => $default_date_format . ' ' . $default_time_format,
		) , $attrs );
		$prefered_datetime_format = $attrs['format'];
		$expired_message = 'This contents already expired';
		$expired_contents = '<div class="pcep contents expired">' .$expired_message .'</div>';
		$today = new DateTime();
		$expireStart = get_post_datetime();
		$expireEnd = null;
		$expireStart = $this->parse_date_time( $attrs['start'] , $default_date_format , $default_time_format , get_post_datetime() );
		$expireEnd = $this->parse_date_time( $attrs['end'] , $default_date_format , $default_time_format , get_post_datetime() );
		if ($expireStart <= $today) {
			if (null === $expireEnd) {
				$contents = $expired_contents;
			} elseif ($expireEnd > $today) {
				$contents = $expired_contents;
			}
		}
		return $contents;
	}
}

add_action( 'plugins_loaded' , 'pcep_partial_contents_expiration_plugin');

function pcep_partial_contents_expiration_plugin() {
	new Partial_Contents_Expiration();
}

?>