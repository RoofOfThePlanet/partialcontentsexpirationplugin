# README #

This wordpress plugins offers you a simple protection for expired contents.


### How do I get set up? ###

Simply, download php file and pack them into zip archive. then, go to wordpress admin dashboard and install zip package.

Alternatively, you can set up by uploading php file itself to your wordpress plugins directory.

## Usage ##

Just enclose your contents within \[pcep\] shortcode

example:

\[pcep start='2020-01-01' end='2020-12-31 format='Y-m-d'] Your Contents \[/pcep]

In this example, {Your Contents} will disappear from 2020-01-01 00:00 till 2020-12-31 00:00

## Shorcode and attributes

### \[pcep]
Enclose your sensitive contents with \[pcep] shortcode

### format
format attribute specify date time format.

### start
start attribute specify first day that your contents will disappear.

### end
end attribute specify last day that your contents will appear again.
if end attribute doesn't exist, your contents permanently invisible from start day.


# License
This plugin is licensed under Apache License. License details are shown below.

---

Copyright 2019-2020 RoofOfThePlanet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  [http://www.apache.org/licenses/LICENSE-2.0]

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

